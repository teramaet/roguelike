﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public static GameController Instance;
	public bool isPlayerTurn;
	public bool areEnemiesMoving;
	public int playerCurrentHealth = 50;
	public AudioClip gameOverSound;
	public int charSelect;
	public Text healthText;
	public int difficultySelect;
	
	private BoardController boardController;
	private List<Enemy> enemies;
	private GameObject SplashScreen;
	private GameObject difficultySelection;
	private GameObject levelImage;
	private GameObject StartScreen;
	private Text levelText;
	private bool settingUpGame;
	private int secondsUntilLevelStart = 2;
	private int secondsUntilLevelStatr = 1;
	private int currentLevel = 1;

	void Awake () {
		if (Instance != null && Instance != this)
		{
			DestroyImmediate(gameObject);
			return;
		}

		Instance = this;
		DontDestroyOnLoad (gameObject);
		boardController = GetComponent<BoardController> ();
		enemies = new List<Enemy> ();
	}

	void Start()
	{
		InitializeSplashScreen ();
	}

	public void HoboButton() {
		charSelect = 1;
		DisableStartScreen ();
	}

	public void KnightButton() {
		playerCurrentHealth += 30;
		charSelect = 0;
		DisableStartScreen ();
	}

	public void Easy()
	{
		difficultySelect = 0;
		InitializeStartScreen ();
	}
	
	public void Medium()
	{
		difficultySelect = 1;
		InitializeStartScreen ();
	}
	
	public void Hard()
	{
		difficultySelect = 2;
		InitializeStartScreen ();
	}

	private void InitializeSplashScreen()
	{
		SplashScreen = GameObject.Find ("Splash Screen");
		SplashScreen.SetActive (true);
		difficultySelection = GameObject.Find ("Difficulty Select");
		difficultySelection.SetActive (false);
		StartScreen = GameObject.Find ("Start Screen");
		StartScreen.SetActive (false);
		levelText = GameObject.Find("Level Text").GetComponent<Text>();
		levelText.text = "Roguelike";
		Invoke ("DisableSplashScreen", secondsUntilLevelStart);
	}

	private void InitializeDifficultyScreen()
	{
		difficultySelection.SetActive (true);
	}
	private void InitializeStartScreen()
	{
		difficultySelection.SetActive (false);
		StartScreen.SetActive (true);
	}

	private void InitializeGame()
	{
		if (currentLevel > 1) 
		{
			StartScreen = GameObject.Find ("Start Screen");
			difficultySelection = GameObject.Find ("Difficulty Select");
			StartScreen.SetActive (false);
			difficultySelection.SetActive(false);
		}
		healthText = GameObject.Find ("Text").GetComponent<Text>();
		healthText.text = "Health: " + playerCurrentHealth;
		settingUpGame = true;
		levelImage = GameObject.Find("Level Image");
		levelText = GameObject.Find("Level Text").GetComponent<Text>();
		levelText.text = "Day " + currentLevel;
		levelImage.SetActive(true);
		enemies.Clear();
		boardController.SetupLevel(currentLevel);
		Invoke ("DisableLevelImage", secondsUntilLevelStart);


	}

	private void DisableLevelImage()
	{
		levelImage.SetActive (false);
		settingUpGame = false;
		isPlayerTurn = true;
		areEnemiesMoving = false;
	}
	private void DisableStartScreen()
	{
		StartScreen = GameObject.Find ("Start Screen");
		StartScreen.SetActive (false);
		settingUpGame = true;
		isPlayerTurn = true;
		areEnemiesMoving = false;
		Invoke ("InitializeGame", secondsUntilLevelStatr);
	}

	private void DisableSplashScreen()
	{
		SplashScreen = GameObject.Find ("Splash Screen");
		SplashScreen.SetActive (false);
		levelText = GameObject.Find("Level Text").GetComponent<Text>();
		levelText.text = "";
		InitializeDifficultyScreen ();
	}


	private void OnLevelWasLoaded(int levelLoaded)
	{
		currentLevel++;
		InitializeGame();
	}

	void Update () {
		if (isPlayerTurn | areEnemiesMoving || settingUpGame) 
		{
			return;
		}

		StartCoroutine (MoveEnemies ());
	}
	private IEnumerator MoveEnemies()
	{
		areEnemiesMoving = true;

		yield return new WaitForSeconds (0.2f);

		foreach (Enemy enemy in enemies) 
		{
			enemy.MoveEnemy();
			yield return new WaitForSeconds(enemy.moveTime);
		}

		areEnemiesMoving = false;
		isPlayerTurn = true;
	}
	public void AddEnemyToList(Enemy enemy)
	{
		enemies.Add (enemy);
	}
	public void GameOver()
	{
		isPlayerTurn = false;
		SoundController.Instance.music.Stop ();
		SoundController.Instance.PlaySingle (gameOverSound);
		levelImage.SetActive (true);
		levelText.text = "You starved after " + currentLevel + " days...";
		enabled = false;
	}
}
	